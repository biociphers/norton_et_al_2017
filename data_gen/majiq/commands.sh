#!/bin/sh

# This script will run all the MAJIQ builder, delta psi, and VOILA commands
# used to generate the data presented in Norton et al Bioinformatics 2017.
# Feel free to override these environment variables:

MMUS=${MAJIQ_MMUS:-/data/DB/mm10/ensembl.mm10.gff3}
HSAP=${MAJIQ_HSAP:-/data/DB/hg19/ensembl.hg19.gff3}
HOGENESCH=${MAJIQ_HOGENESCH:-/data/Hogenesch/bam}
MGP=${MAJIQ_MGP:-/data/MGP/bam}
GTEX=${MAJIQ_GTEX:-/data/GTEX/bam}
SUBSAMPLES=${MAJIQ_SUBSAMPLES:-/data/Hogenesch/Subsample}
SIMUATION=${MAJIQ_SIMULATION:-/data/Simulated}
SIM_NAIVE=${MAJIQ_SIM_NAIVE:-${SIMULATION}/Naive}
SIM_REALISTIC=${MAJIQ_SIM_REALISTIC:-${SIMULATION}/Realistic}
SIM_PERTURBED=${MAJIQ_SIM_PERTURBED:-${SIMULATION}/Perturbed}

# These variables are used for the builder.

NONDENOVO=--nondenovo
NTHREADS=16

# These variables are used for the quantifier.

LW_FLAGS="--weights Auto Auto --weights_local 0.05 --weights_alpha 15"
LA_FLAGS="--weights Auto Auto --weights_local 0.05 --weights_alpha  5"
RLX_FLAGS="--minreads 2 --minpos 2"

# These variables are used for VOILA.

VOILA_FLAGS="--no-html --show-all"

# These functions will make the code a bit cleaner.

build () {
    majiq build --nthreads $NTHREADS -conf $1 $2 --output $3
}

deltapsi () {
    majiq deltapsi --nthreads $NTHREADS -grp1 $1 -grp2 $2 --output $3 \
            --names $4 $5 $6
}

_voila () {
    voila deltapsi $1 -o $(dirname $1) ${VOILA_FLAGS}
}

# Build Hogenesch
build majiq/config/Hogenesch.ini ${MMUS} majiq/build/Hogenesch

# Set up replicates for control
CER_GP=(majiq/build/Hogenesch/Cer_CT{28,34,40}*.majiq.hdf5)
LIV_GP=(majiq/build/Hogenesch/Liv_CT{28,34,40}*.majiq.hdf5)

# Control, No Weights
deltapsi "${CER_GP[*]}" "${LIV_GP[*]}" \
        majiq/deltapsi/Hogenesch/Control/NoWeights Cer Liv

# Control, Local Weights
deltapsi "${CER_GP[*]}" "${LIV_GP[*]}" \
        majiq/deltapsi/Hogenesch/Control/LocalWeights Cer Liv \
        "${LW_FLAGS}"

# Low Alpha
deltapsi "${CER_GP[*]}" "${LIV_GP[*]}" \
        majiq/deltapsi/Hogenesch/Control/LowAlpha Cer Liv \
        "${LA_FLAGS}"


# Set up replicates for set 2
CER_GP=(majiq/build/Hogenesch/Cer_CT{46,52,58}*.majiq.hdf5)
LIV_GP=(majiq/build/Hogenesch/Liv_CT{46,52,58}*.majiq.hdf5)

# Set 2, No Weights
deltapsi "${CER_GP[*]}" "${LIV_GP[*]}" \
        majiq/deltapsi/Hogenesch/Set2/NoWeights Cer Liv \
        "${RLX_FLAGS}"

# Set 2, Local Weights
deltapsi "${CER_GP[*]}" "${LIV_GP[*]}" \
        majiq/deltapsi/Hogenesch/Set2/LocalWeights Cer Liv \
        "${LW_FLAGS} ${RLX_FLAGS}"

deltapsi "${CER_GP[*]}" "${LIV_GP[*]}" \
        majiq/deltapsi/Hogenesch/Set2/LowAlpha Cer Liv \
        "${LA_FLAGS} ${RLX_FLAGS}"

# Swaps
for i in {0..2}
do
    CER_GP=(majiq/build/Hogenesch/Cer_CT{28,34,40}*.majiq.hdf5)
    LIV_GP=(majiq/build/Hogenesch/Liv_CT{28,34,40}*.majiq.hdf5)
    CER_GP[i]=majiq/build/Hogenesch/Mus_CT$((28 + 6 * i))*.majiq.hdf5
    deltapsi "${CER_GP[*]}" "${LIV_GP[*]}" \
             majiq/deltapsi/Hogenesch/Swap/$((i + 1))/NoWeights Cer Liv
    deltapsi "${CER_GP[*]}" "${LIV_GP[*]}" \
             majiq/deltapsi/Hogenesch/Swap/$((i + 1))/LocalWeights Cer Liv \
             "${LW_FLAGS}"
    deltapsi "${CER_GP[*]}" "${LIV_GP[*]}" \
             majiq/deltapsi/Hogenesch/Swap/$((i + 1))/LowAlpha Cer Liv \
             "${LA_FLAGS}"
done

# Subsamples
for ratio in 0.25 0.5 1.00
do
    sed 's/RATIO/${ratio}/g' majiq/config/Subsample.template.ini \
            > majiq/config/Subsample.${ratio}.ini
    build majiq/config/Subsample.${ratio}.ini ${MMUS} \
            majiq/build/Subsample.${ratio}

    CER_GP=(majiq/build/Subsample.${ratio}/Cer_CT{28,34,40}*.majiq.hdf5)
    LIV_GP=(majiq/build/Subsample.${ratio}/Liv_CT{28,34,40}*.majiq.hdf5)
    deltapsi "${CER_GP[*]}" "${LIV_GP[*]}" \
            majiq/deltapsi/Subsample.${ratio}/Set1/NoWeights Cer Liv
    deltapsi "${CER_GP[*]}" "${LIV_GP[*]}" \
            majiq/deltapsi/Subsample.${ratio}/Set1/LocalWeights Cer Liv \
            "${LW_FLAGS}"
    deltapsi "${CER_GP[*]}" "${LIV_GP[*]}" \
            majiq/deltapsi/Subsample.${ratio}/Set1/NoWeightsRelaxed \
            Cer Liv "${RLX_FLAGS}"
    deltapsi "${CER_GP[*]}" "${LIV_GP[*]}" \
            majiq/deltapsi/Subsample.${ratio}/Set1/LocalWeightsRelaxed \
            Cer Liv "${LW_FLAGS} ${RLX_FLAGS}"

    CER_GP=(majiq/build/Hogenesch/Cer_CT{46,52,58}*.majiq.hdf5)
    LIV_GP=(majiq/build/Hogenesch/Liv_CT{46,52,58}*.majiq.hdf5)
    deltapsi "${CER_GP[*]}" "${LIV_GP[*]}" \
            majiq/deltapsi/Subsample.${ratio}/Set2/NoWeights \
            Cer Liv "${RLX_FLAGS}"
    deltapsi "${CER_GP[*]}" "${LIV_GP[*]}" \
            majiq/deltapsi/Subsample.${ratio}/Set2/LocalWeights \
            Cer Liv "${LW_FLAGS} ${RLX_FLAGS}"
done

# No Signal Biological
deltapsi "majiq/build/Hogenesch/Cer_CT{22,28}*.majiq.hdf5" \
        "majiq/build/Hogenesch/Cer_CT{46,52}*.majiq.hdf5" \
        majiq/deltapsi/Hogenesch/NoSignal_Cer Cer1 Cer2
deltapsi "majiq/build/Hogenesch/Liv_CT{22,28}*.majiq.hdf5" \
        "majiq/build/Hogenesch/Liv_CT{46,52}*.majiq.hdf5" \
        majiq/deltapsi/Hogenesch/NoSignal_Liv Liv1 Liv2
deltapsi "majiq/build/Hogenesch/Cer_CT{22,28}*.majiq.hdf5" \
        "majiq/build/Hogenesch/Liv_CT{22,28}*.majiq.hdf5" \
        majiq/deltapsi/Hogenesch/SignalSet1 Cer Liv
deltapsi "majiq/build/Hogenesch/Cer_CT{46,52}*.majiq.hdf5" \
        "majiq/build/Hogenesch/Liv_CT{46,52}*.majiq.hdf5" \
        majiq/deltapsi/Hogenesch/SignalSet2 Cer Liv "${RLX_FLAGS}"

# No Signal Technical (depends on subsampling)
deltapsi "majiq/build/Subsample.0.5/Cer_CT{28,34,40}_1*.majiq.hdf5" \
        "majiq/build/Subsample.0.5/Cer_CT{28,34,40}_2*.majiq.hdf5" \
        majiq/deltapsi/Hogenesch/NoSignalTechnical_Cer_NoWeights \
        Cer1 Cer2
deltapsi "majiq/build/Subsample.0.5/Cer_CT{28,34,40}_1*.majiq.hdf5" \
        "majiq/build/Subsample.0.5/Cer_CT{28,34,40}_2*.majiq.hdf5" \
        majiq/deltapsi/Hogenesch/NoSignalTechnical_Cer_LocalWeights \
        Cer1 Cer2 "${LW_FLAGS}"
deltapsi "majiq/build/Subsample.0.5/Liv_CT{28,34,40}_1*.majiq.hdf5" \
        "majiq/build/Subsample.0.5/Liv_CT{28,34,40}_2*.majiq.hdf5" \
        majiq/deltapsi/Hogenesch/NoSignalTechnical_Liv_NoWeights \
        Liv1 Liv2
deltapsi "majiq/build/Subsample.0.5/Liv_CT{28,34,40}_1*.majiq.hdf5" \
        "majiq/build/Subsample.0.5/Liv_CT{28,34,40}_2*.majiq.hdf5" \
        majiq/deltapsi/Hogenesch/NoSignalTechnical_Liv_LocalWeights \
        Liv1 Liv2 "${LW_FLAGS}"
deltapsi "majiq/build/Subsample.0.5/Cer_CT{28,34,40}_1*.majiq.hdf5" \
        "majiq/build/Subsample.0.5/Liv_CT{28,34,40}_1*.majiq.hdf5" \
        majiq/deltapsi/Hogenesch/SignalTechnical_Set1_NoWeights \
        Cer Liv
deltapsi "majiq/build/Subsample.0.5/Cer_CT{28,34,40}_1*.majiq.hdf5" \
        "majiq/build/Subsample.0.5/Liv_CT{28,34,40}_1*.majiq.hdf5" \
        majiq/deltapsi/Hogenesch/SignalTechnical_Set1_LocalWeights \
        Cer Liv "${LW_FLAGS}"
deltapsi "majiq/build/Subsample.0.5/Cer_CT{28,34,40}_2*.majiq.hdf5" \
        "majiq/build/Subsample.0.5/Liv_CT{28,34,40}_2*.majiq.hdf5" \
        majiq/deltapsi/Hogenesch/SignalTechnical_Set2_NoWeights \
        Cer Liv "${RLX_FLAGS}"
deltapsi "majiq/build/Subsample.0.5/Cer_CT{28,34,40}_2*.majiq.hdf5" \
        "majiq/build/Subsample.0.5/Liv_CT{28,34,40}_2*.majiq.hdf5" \
        majiq/deltapsi/Hogenesch/SignalTechnical_Set2_LocalWeights \
        Cer Liv "${RLX_FLAGS} ${LW_FLAGS}"

# MGP
build majiq/config/MGP.ini ${MMUS} majiq/build/MGP

LUN_GRP=(majiq/build/MGP/Lung[1-3]*.majiq.hdf5)
LIV_GRP=(majiq/build/MGP/Liver[1-3]*.majiq.hdf5)
deltapsi "${LUN_GRP}" "${LIV_GRP}" majiq/deltapsi/MGP/Control/NoWeights \
        Lung Liver
deltapsi "${LUN_GRP}" "${LIV_GRP}" majiq/deltapsi/MGP/Control/LocalWeights \
        Lung Liver "${LW_FLAGS}"

LUN_GRP=(majiq/build/MGP/Lung[4-6]*.majiq.hdf5)
LIV_GRP=(majiq/build/MGP/Liver[4-6]*.majiq.hdf5)
deltapsi "${LUN_GRP}" "${LIV_GRP}" majiq/deltapsi/MGP/Set2/NoWeights \
        Lung Liver "${RLX_FLAGS}"
deltapsi "${LUN_GRP}" "${LIV_GRP}" majiq/deltapsi/MGP/Set2/LocalWeights \
        Lung Liver "${LW_FLAGS} ${RLX_FLAGS}"

for i in {0..2}
do
    LUN_GRP=(majiq/build/MGP/Lung[1-3]*.majiq.hdf5)
    LIV_GRP=(majiq/build/MGP/Liver[1-3]*.majiq.hdf5)
    LUN_GRP[0]=majiq/build/MGP/Hippocampus$((i+1))*.majiq.hdf5
    deltapsi "${LUN_GRP}" "${LIV_GRP}" \
            majiq/deltapsi/MGP/Swap/$((i+1))/NoWeights Lung Liver
    deltapsi "${LUN_GRP}" "${LIV_GRP}" \
            majiq/deltapsi/MGP/Swap/$((i+1))/LocalWeights Lung Liver \
            "${LW_FLAGS}"
done

# Simulation
for i in {1..3}
do
    python majiq/pyscripts/sample_replicates.py \
            -grp1 "${SIM_NAIVE}/Hippocampus*.bam" \
            -grp2 "${SIM_NAIVE}/Liver*.bam" \
            -t majiq/config/Simulation.template.ini \
            -o majiq/config/Simulation.naive.ini > sampled_idxs.txt
    build majiq/config/Simulation.naive.ini ${MMUS} majiq/build/SimNaive${i}
    deltapsi "majiq/build/SimNaive${i}/Hippocampus*.majiq.hdf5" \
             "majiq/build/SimNaive${i}/Liver*.majiq.hdf5" \
             majiq/deltapsi/SimNaive${i}/Control_NoWeights \
             Hippocampus Liver
    deltapsi "majiq/build/SimNaive${i}/Hippocampus*.majiq.hdf5" \
             "majiq/build/SimNaive${i}/Liver*.majiq.hdf5" \
             majiq/deltapsi/SimNaive${i}/Control_LocalWeights \
             Hippocampus Liver "${LW_FLAGS}"
    python majiq/pyscripts/sample_replicates.py \
            -grp1 "${SIM_REALISTIC}/Hippocampus*.bam" \
            -grp2 "${SIM_REALISTIC}/Liver*.bam" \
            -t majiq/config/Simulation.template.ini \
            -o majiq/config/Simulation.realistic.ini \
            --presamp sampled_idxs.txt
    build majiq/config/Simulation.realistic.ini ${MMUS} \
            majiq/build/SimRealistic${i}
    deltapsi "majiq/build/SimRealistic${i}/Hippocampus*.majiq.hdf5" \
             "majiq/build/SimRealistic${i}/Liver*.majiq.hdf5" \
             majiq/deltapsi/SimRealistic${i}/Control_NoWeights \
             Hippocampus Liver
    deltapsi "majiq/build/SimRealistic${i}/Hippocampus*.majiq.hdf5" \
             "majiq/build/SimRealistic${i}/Liver*.majiq.hdf5" \
             majiq/deltapsi/SimRealistic${i}/Control_LocalWeights \
             Hippocampus Liver "${LW_FLAGS}"
    deltapsi "majiq/build/SimRealistic${i}/Hippocampus*.majiq.hdf5" \
             "majiq/build/SimRealistic${i}/Liver*.majiq.hdf5" \
             majiq/deltapsi/SimRealistic${i}/Control_LowAlpha \
             Hippocampus Liver "${LA_FLAGS}"
    python majiq/pyscripts/sample_replicates.py \
            -grp1 "${SIM_REALISTIC}/Hippocampus*.bam" \
            -grp2 "${SIM_REALISTIC}/Liver*.bam" \
            -grpp "${SIM_PERTURBED}/Hippocampus*.bam" \
            -t majiq/config/Simulation.template.ini \
            -o majiq/config/Simulation.perturbed.ini \
            --presamp sampled_idxs.txt
    build majiq/config/Simulation.perturbed.ini ${MMUS} \
            majiq/build/SimRealistic${i}
    deltapsi "majiq/build/SimPerturbed${i}/Hippocampus*.majiq.hdf5" \
             "majiq/build/SimPerturbed${i}/Liver*.majiq.hdf5" \
             majiq/deltapsi/SimPerturbed${i}/Control_NoWeights \
             Hippocampus Liver
    deltapsi "majiq/build/SimPerturbed${i}/Hippocampus*.majiq.hdf5" \
             "majiq/build/SimPerturbed${i}/Liver*.majiq.hdf5" \
             majiq/deltapsi/SimPerturbed${i}/Control_LocalWeights \
             Hippocampus Liver "${LW_FLAGS}"
    deltapsi "majiq/build/SimPerturbed${i}/Hippocampus*.majiq.hdf5" \
             "majiq/build/SimPerturbed${i}/Liver*.majiq.hdf5" \
             majiq/deltapsi/SimPerturbed${i}/Control_LowAlpha \
             Hippocampus Liver "${LA_FLAGS}"
    python majiq/pyscripts/sample_replicates.py \
            -grp1 "${SIM_REALISTIC}/Hippocampus*.bam" \
            -grp2 "${SIM_REALISTIC}/Liver*.bam" \
            -t majiq/config/Simulation.template.ini \
            -o majiq/config/Simulation.swapped.ini \
            --presamp sampled_idxs.txt --swap
    build majiq/config/Simulation.swapped.ini ${MMUS} \
            majiq/build/SimRealistic${i}
    deltapsi "majiq/build/SimSwapped${i}/Hippocampus*.majiq.hdf5" \
             "majiq/build/SimSwapped${i}/Liver*.majiq.hdf5" \
             majiq/deltapsi/SimSwapped${i}/Control_NoWeights \
             Hippocampus Liver
    deltapsi "majiq/build/SimSwapped${i}/Hippocampus*.majiq.hdf5" \
             "majiq/build/SimSwapped${i}/Liver*.majiq.hdf5" \
             majiq/deltapsi/SimSwapped${i}/Control_LocalWeights \
             Hippocampus Liver "${LW_FLAGS}"
    deltapsi "majiq/build/SimSwapped${i}/Hippocampus*.majiq.hdf5" \
             "majiq/build/SimSwapped${i}/Liver*.majiq.hdf5" \
             majiq/deltapsi/SimSwapped${i}/Control_LowAlpha \
             Hippocampus Liver "${LA_FLAGS}"
done

# VOILA TSV files
find majiq/deltapsi -iname "*.deltapsi.voila" -exec _voila {} \;
