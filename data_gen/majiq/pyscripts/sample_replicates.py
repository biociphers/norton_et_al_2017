import argparse
import configparser
import os

import numpy as np


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-grp1', nargs='+', required='True')
    parser.add_argument('-grp2', nargs='+', required='True')
    parser.add_argument('-grpp', nargs='?')
    parser.add_argument('-n', dest='nsamps', type=int, default=3)
    parser.add_argument('-o', dest='output', type=argparse.FileType('w'),
                        required='True')
    parser.add_argument('-p', dest='paired', action='store_true')
    parser.add_argument('-t', dest='template', required='True')
    parser.add_argument('--presamp', type=argparse.FileType())
    parser.add_argument('--swap', action='store_true')
    args = parser.parse_args()
    samdir = os.path.split(args.grp1[0])[0]
    all_f = args.grp1 + args.grp2 + (args.grpp if args.grpp else [])
    assert all([os.path.split(fn)[0] == samdir for fn in all_f]), \
            ValueError('all files must be in same directory!')
    assert all([fn.endswith('.bam') for fn in all_f]), \
            ValueError('all files must be BAM files')
    if args.presamp:
        idxs1 = map(int, args.presamp.readline().split())
        idxs2 = map(int, args.presamp.readline().split())
    else:
        idxs1 = np.random.choice(len(args.grp1), args.nsamps, replace=False)
        if args.paired:
            assert len(args.grp1) == len(args.grp2), \
                ValueError('"-p" requires both groups to have same length')
            idxs2 = idxs1.copy()
        else:
            idxs2 = np.random.choice(len(args.grp2), args.nsamps,
                                     replace=False)
    print ' '.join(map(str, idxs1))
    print ' '.join(map(str, idxs2))
    grp1 = [os.path.split(args.grp1[i])[1].replace('.bam', '') for i in idxs1]
    grp2 = [os.path.split(args.grp2[i])[1].replace('.bam', '') for i in idxs2]
    if args.grpp:
        grp1[0] = args.grpp[idxs1[0]]
    elif args.swap:
        sidx = np.random.choice(len(args.grp2))
        while sidx in idxs2:
            sidx = np.random.choice(len(args.grp2))
        grp1[0] = args.grp2[sidx]

    config = configparser.SafeConfigParser()
    config.read(args.template)
    config.set('info', 'samdir', samdir)
    names = config['experiments'].keys()
    config.set('experiments', names[0], ','.join(grp1))
    config.set('experiments', names[1], ','.join(grp2))
    config.write(args.output, space_around_delimiters=False)

if __name__ == '__main__':
    main()
