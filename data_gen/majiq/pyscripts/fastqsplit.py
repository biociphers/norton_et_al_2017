import argparse
import os
import pysam

import numpy as np


def makedirs(path):
    if not os.path.exists(path):
        os.makedirs(path)
    return path

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('mate1', type=pysam.FastxFile)
    parser.add_argument('mate2', type=pysam.FastxFile)
    parser.add_argument('outdir', type=makedirs)
    parser.add_argument('nfolds', nargs='?', type=int, default=2)
    args = parser.parse_args()

    sampled_idxs = []

    if args.mate1.filename.endswith('gz'):
        src, ext = os.path.splitext(os.path.splitext(args.mate1.filename)[0])
    else:
        src, ext = os.path.splitext(args.mate1.filename)
    src = os.path.join(args.outdir, os.path.split(src)[1])
    outfiles = []
    try:
        for i in range(args.nfolds):
            outfiles.append(open('{}_{}{}'.format(src, i + 1, ext), 'w'))
        for read in args.mate1:
            sampled_idxs.append(np.random.randint(args.nfolds))
            cur_o = outfiles[sampled_idxs[-1]]
            cur_o.write(str(read) + '\n')
    finally:
        [outfile.close() for outfile in outfiles]

    if args.mate2.filename.endswith('gz'):
        src, ext = os.path.splitext(os.path.splitext(args.mate2.filename)[0])
    else:
        src, ext = os.path.splitext(args.mate2.filename)
    src = os.path.join(args.outdir, os.path.split(src)[1])
    outfiles = []
    try:
        for i in range(args.nfolds):
            outfiles.append(open('{}_{}{}'.format(src, i + 1, ext), 'w'))
        for ridx, read in enumerate(args.mate2):
            cur_o = outfiles[sampled_idxs[ridx]]
            cur_o.write(str(read) + '\n')
    finally:
        [outfile.close() for outfile in outfiles]
