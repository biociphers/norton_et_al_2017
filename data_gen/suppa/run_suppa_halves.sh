 evts="halves"
 exec_quant="halves/quant_for_suppa"
 s_events="halves/suppa_events"
 outDir="output/halves"
 
 mkdir -p $evts
 mkdir -p $exec_quant
 mkdir -p $s_events
 mkdir -p $outDir
 
##2. Extract the TPM values from the Salmon output
python ~/software/suppa/multipleFieldSelection.py -i quantification/0.5/*/quant.sf -k 1 -f 4 -o $exec_quant/iso_tpm.txt
#
#echo "AFTER MULTIPLE"
#3. Before running SUPPA, we need to calculate the AS events on the hg19 annotation
        #3.1: Generate the events: 
        python ~/software/suppa/suppa.py generateEvents -i /data/DB/mm10/forSUPPA/refseq_mm10.formatted.gtf -o ./$s_events/events -e SE SS MX RI FL
        #3.2: Put all the ioe events in the same file:
        dir="./$s_events/"
        for file in $(ls $dir | grep .ioe);do
                echo "Processing $file..."
                cat $dir/$file >> $dir/summary.events_formatted.ioe
        done
        #3.3: Put all the gtf events in the same file:
        for file in $(ls $dir | grep .gtf);do
                echo "Processing $file..."
                cat $dir/$file >> $dir/summary.events_formatted.gtf
        done

#4. Run SUPPA for getting the psi values of the events:
python ~/software/suppa/suppa.py psiPerEvent -i $s_events/summary.events_formatted.ioe -e $exec_quant/iso_tpm.txt -o $evts/events
#
#5. Run SUPPA for obtaining the Differential splicing analysis

        python split_file.py $exec_quant/iso_tpm.txt -c Cer_CT28_1 Cer_CT34_1 Cer_CT40_1 -o $exec_quant/Cer1_28_34_40_iso.tpm
        python split_file.py $evts/events.psi -c Cer_CT28_1 Cer_CT34_1 Cer_CT40_1 -o $exec_quant/Cer1_28_34_40_events.psi

        python split_file.py $exec_quant/iso_tpm.txt -c Liv_CT28_1 Liv_CT34_1 Liv_CT40_1 -o $exec_quant/Liv1_28_34_40_iso.tpm
        python split_file.py $evts/events.psi -c Liv_CT28_1 Liv_CT34_1 Liv_CT40_1 -o $exec_quant/Liv1_28_34_40_events.psi

        python ~/software/suppa/suppa.py diffSplice -m empirical -i $s_events/summary.events_formatted.ioe -e $exec_quant/Cer1_28_34_40_iso.tpm $exec_quant/Liv1_28_34_40_iso.tpm -p $exec_quant/Cer1_28_34_40_events.psi $exec_quant/Liv1_28_34_40_events.psi -o $outDir/Control_dpsi1

        python split_file.py $exec_quant/iso_tpm.txt -c Cer_CT28_2 Cer_CT34_2 Cer_CT40_2 -o $exec_quant/Cer2_28_34_40_iso.tpm
        python split_file.py $evts/events.psi -c Cer_CT28_2 Cer_CT34_2 Cer_CT40_2 -o $exec_quant/Cer2_28_34_40_events.psi

        python split_file.py $exec_quant/iso_tpm.txt -c Liv_CT28_2 Liv_CT34_2 Liv_CT40_2 -o $exec_quant/Liv2_28_34_40_iso.tpm
        python split_file.py $evts/events.psi -c Liv_CT28_2 Liv_CT34_2 Liv_CT40_2 -o $exec_quant/Liv2_28_34_40_events.psi

        python ~/software/suppa/suppa.py diffSplice -m empirical -i $s_events/summary.events_formatted.ioe -e $exec_quant/Cer2_28_34_40_iso.tpm $exec_quant/Liv2_28_34_40_iso.tpm -p $exec_quant/Cer2_28_34_40_events.psi $exec_quant/Liv2_28_34_40_events.psi -o  $outDir/Control_dpsi2
       
## NOSIGNAL Technical replicates
        echo NOSIGNAL Cer
        python split_file.py $exec_quant/iso_tpm.txt -c Cer_CT28_1 Cer_CT34_1 Cer_CT40_1 -o $exec_quant/nosignal_Cer1_iso.tpm
        python split_file.py $evts/events.psi -c  Cer_CT28_1 Cer_CT34_1 Cer_CT40_1 -o $exec_quant/nosignal_Cer1_events.psi

        python split_file.py $exec_quant/iso_tpm.txt -c Cer_CT28_2 Cer_CT34_2 Cer_CT40_2 -o $exec_quant/nosignal_Cer2_iso.tpm
        python split_file.py $evts/events.psi -c  Cer_CT28_2 Cer_CT34_2 Cer_CT40_2 -o $exec_quant/nosignal_Cer2_events.psi

        python ~/software/suppa/suppa.py diffSplice -m empirical -i $s_events/summary.events_formatted.ioe -e $exec_quant/nosignal_Cer1_iso.tpm $exec_quant/nosignal_Cer2_iso.tpm -p $exec_quant/nosignal_Cer1_events.psi $exec_quant/nosignal_Cer2_events.psi -o $outDir/nosignal_Cer

        echo NOSIGNAL Liv
        python split_file.py $exec_quant/iso_tpm.txt -c Liv_CT28_1 Liv_CT34_1 Liv_CT40_1 -o $exec_quant/nosignal_Liv1_iso.tpm
        python split_file.py $evts/events.psi -c  Liv_CT28_1 Liv_CT34_1 Liv_CT40_1 -o $exec_quant/nosignal_Liv1_events.psi
 
        python split_file.py $exec_quant/iso_tpm.txt -c Liv_CT28_2 Liv_CT34_2 Liv_CT40_2 -o $exec_quant/nosignal_Liv2_iso.tpm
        python split_file.py $evts/events.psi -c  Liv_CT28_2 Liv_CT34_2 Liv_CT40_2 -o $exec_quant/nosignal_Liv2_events.psi

        python ~/software/suppa/suppa.py diffSplice -m empirical -i $s_events/summary.events_formatted.ioe -e $exec_quant/nosignal_Liv1_iso.tpm $exec_quant/nosignal_Liv2_iso.tpm -p $exec_quant/nosignal_Liv1_events.psi $exec_quant/nosignal_Liv2_events.psi -o $outDir/nosignal_Liv
