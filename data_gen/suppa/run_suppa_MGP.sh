 evts="MGP"
 exec_quant="MGP/quant_for_suppa"
 s_events="MGP/suppa_events"
 outDir="output/MGP"
 
 mkdir -p $evts
 mkdir -p $exec_quant
 mkdir -p $s_events
 mkdir -p $outDir

##2. Extract the TPM values from the Salmon output
python ~/software/suppa/multipleFieldSelection.py -i quantification/MGP/*/quant.sf -k 1 -f 4 -o $exec_quant/iso_tpm.txt
#
#echo "AFTER MULTIPLE"
#3. Before running SUPPA, we need to calculate the AS events on the hg19 annotation
        #3.1: Generate the events: 
        python ~/software/suppa/suppa.py generateEvents -i /data/DB/mm10/forSUPPA/refseq_mm10.formatted.gtf -o ./$s_events/events -e SE SS MX RI FL
        #3.2: Put all the ioe events in the same file:
        dir="./$s_events/"
        for file in $(ls $dir | grep .ioe);do
                echo "Processing $file..."
                cat $dir/$file >> $dir/summary.events_formatted.ioe
        done
        #3.3: Put all the gtf events in the same file:
        for file in $(ls $dir | grep .gtf);do
                echo "Processing $file..."
                cat $dir/$file >> $dir/summary.events_formatted.gtf
        done

#4. Run SUPPA for getting the psi values of the events:
python ~/software/suppa/suppa.py psiPerEvent -i $s_events/summary.events_formatted.ioe -e $exec_quant/iso_tpm.txt -o $evts/events
#
#5. Run SUPPA for obtaining the Differential splicing analysis
        #11.1: Split the PSI and TPM files between the 2 conditions
        #Set 1 group 1: Lung1, Lung2, Lung3 
        #Set 1 group 2: Liv1, Liv2, Liv3 
        #Set 2 group 1: Lung4, Lung5, Lung6
        #Set 2 group 2: Liv4, Liv5, Liv6


        ## CONTROL_$outDir = heart (1, 3, and 5)  vs liver (1, 2, and 3) in MGP.
        python split_file.py $exec_quant/iso_tpm.txt -c Lung1 Lung2 Lung3 -o $exec_quant/Lung_1_2_3_iso.tpm
        python split_file.py $evts/events.psi -c Lung1 Lung2 Lung3 -o $exec_quant/Lung_1_2_3_events.psi

        python split_file.py $exec_quant/iso_tpm.txt -c Liv1 Liv2 Liv3 -o $exec_quant/Liv_1_2_3_iso.tpm
        python split_file.py $evts/events.psi -c Liv1 Liv2 Liv3 -o $exec_quant/Liv_1_2_3_events.psi

        python ~/software/suppa/suppa.py diffSplice -m empirical -i $s_events/summary.events_formatted.ioe -e $exec_quant/Lung_1_2_3_iso.tpm $exec_quant/Liv_1_2_3_iso.tpm -p $exec_quant/Lung_1_2_3_events.psi $exec_quant/Liv_1_2_3_events.psi -o $outDir/control

#       
        ##Tissue MGP: In set 1 group 1, remove one Lung and replace with the corresponding Hip (1 2 3). 
	## Perform this procedure four times, once for each circadian timepoint (so Lung1 is replaced with Hip1, etc.), 
	## and estimate a mean and variance for N and RR. Use the same set 1 group 2, set 2, group 1, and set 2 group 2 from the control.

       	python split_file.py $exec_quant/iso_tpm.txt -c Hip1 Lung2 Lung3 -o $exec_quant/Lung_2_3_Hip_1_iso.tpm 
       	python split_file.py $evts/events.psi -c Hip1 Lung2 Lung3 -o $exec_quant/Lung_2_3_Hip_1_events.psi 

       	python split_file.py $exec_quant/iso_tpm.txt -c Lung1 Hip2 Lung3 -o $exec_quant/Lung_1_3_Hip_2_iso.tpm 
       	python split_file.py $evts/events.psi -c Lung1 Hip2 Lung3 -o $exec_quant/Lung_1_3_Hip_2_events.psi 

       	python split_file.py $exec_quant/iso_tpm.txt -c Lung1 Lung2 Hip3 -o $exec_quant/Lung_1_2_Hip_3_iso.tpm 
       	python split_file.py $evts/events.psi -c Lung1 Lung2 Hip3 -o $exec_quant/Lung_1_2_Hip_3_events.psi 

       	python ~/software/suppa/suppa.py diffSplice -m empirical -i $s_events/summary.events_formatted.ioe -e $exec_quant/Lung_2_3_Hip_1_iso.tpm $exec_quant/Liv_1_2_3_iso.tpm -p $exec_quant/Lung_2_3_Hip_1_events.psi $exec_quant/Liv_1_2_3_events.psi -o $outDir/dpsi1
       	
       	python ~/software/suppa/suppa.py diffSplice -m empirical -i $s_events/summary.events_formatted.ioe -e $exec_quant/Lung_1_3_Hip_2_iso.tpm $exec_quant/Liv_1_2_3_iso.tpm -p $exec_quant/Lung_1_3_Hip_2_events.psi $exec_quant/Liv_1_2_3_events.psi -o $outDir/dpsi2
       	
       	python ~/software/suppa/suppa.py diffSplice -m empirical -i $s_events/summary.events_formatted.ioe -e $exec_quant/Lung_1_2_Hip_3_iso.tpm $exec_quant/Liv_1_2_3_iso.tpm -p $exec_quant/Lung_1_2_Hip_3_events.psi $exec_quant/Liv_1_2_3_events.psi -o $outDir/dpsi3

        python split_file.py $exec_quant/iso_tpm.txt -c Lung4 Lung5 Lung6 -o $exec_quant/Lung_4_5_6_iso.tpm
        python split_file.py $evts/events.psi -c Lung4 Lung5 Lung6 -o $exec_quant/Lung_4_5_6_events.psi

        python split_file.py $exec_quant/iso_tpm.txt -c Liv4 Liv5 Liv6 -o $exec_quant/Liv_4_5_6_iso.tpm
        python split_file.py $evts/events.psi -c Liv4 Liv5 Liv6 -o $exec_quant/Liv_4_5_6_events.psi

        python ~/software/suppa/suppa.py diffSplice -m empirical -i $s_events/summary.events_formatted.ioe -e $exec_quant/Lung_4_5_6_iso.tpm $exec_quant/Liv_4_5_6_iso.tpm -p $exec_quant/Lung_4_5_6_events.psi $exec_quant/Liv_4_5_6_events.psi -o $outDir/set2

