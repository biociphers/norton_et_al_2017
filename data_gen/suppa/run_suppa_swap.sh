 evts="swap"
 exec_quant="swap/quant_for_suppa"
 s_events="swap/suppa_events"
 outDir="output/swap"
 
 mkdir -p $evts
 mkdir -p $exec_quant
 mkdir -p $s_events
 mkdir -p $outDir

##2. Extract the TPM values from the Salmon output
python ~/software/suppa/multipleFieldSelection.py -i quantification/1.0/*/quant.sf -k 1 -f 4 -o $exec_quant/iso_tpm.txt
#
#echo "AFTER MULTIPLE"
#3. Before running SUPPA, we need to calculate the AS events on the hg19 annotation
        #3.1: Generate the events: 
        python ~/software/suppa/suppa.py generateEvents -i /data/DB/mm10/forSUPPA/refseq_mm10.formatted.gtf -o ./$s_events/events -e SE SS MX RI FL
        #3.2: Put all the ioe events in the same file:
        dir="./$s_events/"
        for file in $(ls $dir | grep .ioe);do
                echo "Processing $file..."
                cat $dir/$file >> $dir/summary.events_formatted.ioe
        done
        #3.3: Put all the gtf events in the same file:
        for file in $(ls $dir | grep .gtf);do
                echo "Processing $file..."
                cat $dir/$file >> $dir/summary.events_formatted.gtf
        done

#4. Run SUPPA for getting the psi values of the events:
python ~/software/suppa/suppa.py psiPerEvent -i $s_events/summary.events_formatted.ioe -e $exec_quant/iso_tpm.txt -o $evts/events
#
#5. Run SUPPA for obtaining the Differential splicing analysis
        #11.1: Split the PSI and TPM files between the 2 conditions
        #Set 1 group 1: Cer_CT28, Cer_CT34, Cer_CT40 
        #Set 1 group 2: Liv_CT28, Liv_CT34, Liv_CT40 
        #Set 2 group 1: Cer_CT46, Cer_CT52, Cer_CT58
        #Set 2 group 2: Liv_CT46, Liv_CT52, Liv_CT58


        ## CONTROL_$outDir = heart (1, 3, and 5)  vs liver (1, 2, and 3) in MGP.
        python split_file.py $exec_quant/iso_tpm.txt -c Cer_CT28 Cer_CT34 Cer_CT40 -o $exec_quant/Cer_28_34_40_iso.tpm
        python split_file.py $evts/events.psi -c Cer_CT28 Cer_CT34 Cer_CT40 -o $exec_quant/Cer_28_34_40_events.psi

        python split_file.py $exec_quant/iso_tpm.txt -c Liv_CT28 Liv_CT34 Liv_CT40 -o $exec_quant/Liv_28_34_40_iso.tpm
        python split_file.py $evts/events.psi -c Liv_CT28 Liv_CT34 Liv_CT40 -o $exec_quant/Liv_28_34_40_events.psi

        python ~/software/suppa/suppa.py diffSplice -m empirical -i $s_events/summary.events_formatted.ioe -e $exec_quant/Cer_28_34_40_iso.tpm $exec_quant/Liv_28_34_40_iso.tpm -p $exec_quant/Cer_28_34_40_events.psi $exec_quant/Liv_28_34_40_events.psi -o $outDir/control

#       
        ##Tissue swap: In set 1 group 1, remove one Cer and replace with the corresponding Mus (CT28, CT34, CT40). 
	## Perform this procedure four times, once for each circadian timepoint (so Cer_CT28 is replaced with Mus_CT28, etc.),
	##and estimate a mean and variance for N and RR. Use the same set 1 group 2, set 2, group 1, and set 2 group 2 from the control.

       	python split_file.py $exec_quant/iso_tpm.txt -c Mus_CT28 Cer_CT34 Cer_CT40 -o $exec_quant/Cer_34_40_Mus_28_iso.tpm 
       	python split_file.py $evts/events.psi -c Mus_CT28 Cer_CT34 Cer_CT40 -o $exec_quant/Cer_34_40_Mus_28_events.psi 

       	python split_file.py $exec_quant/iso_tpm.txt -c Cer_CT28 Mus_CT34 Cer_CT40 -o $exec_quant/Cer_28_40_Mus_34_iso.tpm 
       	python split_file.py $evts/events.psi -c Cer_CT28 Mus_CT34 Cer_CT40 -o $exec_quant/Cer_28_40_Mus_34_events.psi 

       	python split_file.py $exec_quant/iso_tpm.txt -c Cer_CT28 Cer_CT34 Mus_CT40 -o $exec_quant/Cer_28_34_Mus_40_iso.tpm 
       	python split_file.py $evts/events.psi -c Cer_CT28 Cer_CT34 Mus_CT40 -o $exec_quant/Cer_28_34_Mus_40_events.psi 

       	python ~/software/suppa/suppa.py diffSplice -m empirical -i $s_events/summary.events_formatted.ioe -e $exec_quant/Cer_34_40_Mus_28_iso.tpm $exec_quant/Liv_28_34_40_iso.tpm -p $exec_quant/Cer_34_40_Mus_28_events.psi $exec_quant/Liv_28_34_40_events.psi -o $outDir/dpsi1
       	
       	python ~/software/suppa/suppa.py diffSplice -m empirical -i $s_events/summary.events_formatted.ioe -e $exec_quant/Cer_28_40_Mus_34_iso.tpm $exec_quant/Liv_28_34_40_iso.tpm -p $exec_quant/Cer_28_40_Mus_34_events.psi $exec_quant/Liv_28_34_40_events.psi -o $outDir/dpsi2
       	
       	python ~/software/suppa/suppa.py diffSplice -m empirical -i $s_events/summary.events_formatted.ioe -e $exec_quant/Cer_28_34_Mus_40_iso.tpm $exec_quant/Liv_28_34_40_iso.tpm -p $exec_quant/Cer_28_34_Mus_40_events.psi $exec_quant/Liv_28_34_40_events.psi -o $outDir/dpsi3

        python split_file.py $exec_quant/iso_tpm.txt -c Cer_CT46 Cer_CT52 Cer_CT58 -o $exec_quant/Cer_46_52_58_iso.tpm
        python split_file.py $evts/events.psi -c Cer_CT46 Cer_CT52 Cer_CT58 -o $exec_quant/Cer_46_52_58_events.psi

        python split_file.py $exec_quant/iso_tpm.txt -c Liv_CT46 Liv_CT52 Liv_CT58 -o $exec_quant/Liv_46_52_58_iso.tpm
        python split_file.py $evts/events.psi -c Liv_CT46 Liv_CT52 Liv_CT58 -o $exec_quant/Liv_46_52_58_events.psi

        python ~/software/suppa/suppa.py diffSplice -m empirical -i $s_events/summary.events_formatted.ioe -e $exec_quant/Cer_46_52_58_iso.tpm $exec_quant/Liv_46_52_58_iso.tpm -p $exec_quant/Cer_46_52_58_events.psi $exec_quant/Liv_46_52_58_events.psi -o $outDir/set2

   ## RTPCR
        python split_file.py $exec_quant/iso_tpm.txt -c Cer_CT28 Cer_CT40 Cer_CT52 -o $exec_quant/Cer_28_40_52_iso.tpm
        python split_file.py $evts/events.psi -c Cer_CT28 Cer_CT40 Cer_CT52 -o $exec_quant/Cer_28_40_52_events.psi
 
        python split_file.py $exec_quant/iso_tpm.txt -c Liv_CT28 Liv_CT40 Liv_CT52 -o $exec_quant/Liv_28_40_52_iso.tpm
        python split_file.py $evts/events.psi -c Liv_CT28 Liv_CT40 Liv_CT52 -o $exec_quant/Liv_28_40_52_events.psi
 
        python ~/software/suppa/suppa.py diffSplice -m empirical -i $s_events/summary.events_formatted.ioe -e $exec_quant/Cer_28_40_52_iso.tpm $exec_quant/Liv_28_40_52_iso.tpm -p $exec_quant/Cer_28_40_52_events.psi $exec_quant/Liv_28_40_52_events.psi -o $outDir/rtpcr_control

       	
	python split_file.py $exec_quant/iso_tpm.txt -c Mus_CT28 Cer_CT40 Cer_CT58 -o $exec_quant/Cer_40_52_Mus_28_iso.tpm 
       	python split_file.py $evts/events.psi -c Mus_CT28 Cer_CT40 Cer_CT58 -o $exec_quant/Cer_40_52_Mus_28_events.psi 

       	python split_file.py $exec_quant/iso_tpm.txt -c Cer_CT28 Mus_CT40 Cer_CT58 -o $exec_quant/Cer_28_52_Mus_40_iso.tpm 
       	python split_file.py $evts/events.psi -c Cer_CT28 Mus_CT40 Cer_CT58 -o $exec_quant/Cer_28_52_Mus_40_events.psi 

       	python split_file.py $exec_quant/iso_tpm.txt -c Cer_CT28 Cer_CT40 Mus_CT58 -o $exec_quant/Cer_28_40_Mus_52_iso.tpm 
       	python split_file.py $evts/events.psi -c Cer_CT28 Cer_CT40 Mus_CT58 -o $exec_quant/Cer_28_40_Mus_52_events.psi 

       	python ~/software/suppa/suppa.py diffSplice -m empirical -i $s_events/summary.events_formatted.ioe -e $exec_quant/Cer_40_52_Mus_28_iso.tpm $exec_quant/Liv_28_40_52_iso.tpm -p $exec_quant/Cer_40_52_Mus_28_events.psi $exec_quant/Liv_28_40_52_events.psi -o $outDir/rtpcr_dpsi1
       	python ~/software/suppa/suppa.py diffSplice -m empirical -i $s_events/summary.events_formatted.ioe -e $exec_quant/Cer_28_52_Mus_40_iso.tpm $exec_quant/Liv_28_40_52_iso.tpm -p $exec_quant/Cer_28_52_Mus_40_events.psi $exec_quant/Liv_28_40_52_events.psi -o $outDir/rtpcr_dpsi2
	python ~/software/suppa/suppa.py diffSplice -m empirical -i $s_events/summary.events_formatted.ioe -e $exec_quant/Cer_28_40_Mus_52_iso.tpm $exec_quant/Liv_28_40_52_iso.tpm -p $exec_quant/Cer_28_40_Mus_58_events.psi $exec_quant/Liv_28_40_52_events.psi -o $outDir/rtpcr_dpsi3

   ## NOSIGNAL HOGENESCH
        python split_file.py $exec_quant/iso_tpm.txt -c Cer_CT22 Cer_CT28 -o $exec_quant/Cer_22_28_iso.tpm
        python split_file.py $evts/events.psi -c  Cer_CT22 Cer_CT28  -o $exec_quant/Cer_22_28_events.psi

        python split_file.py $exec_quant/iso_tpm.txt -c Cer_CT34 Cer_CT40 -o $exec_quant/Cer_34_40_iso.tpm
        python split_file.py $evts/events.psi -c Cer_CT34 Cer_CT40 -o $exec_quant/Cer_34_40_events.psi

        python split_file.py $exec_quant/iso_tpm.txt -c Cer_CT46 Cer_CT52 -o $exec_quant/Cer_46_52_iso.tpm
        python split_file.py $evts/events.psi -c  Cer_CT46 Cer_CT52  -o $exec_quant/Cer_46_52_events.psi

        python split_file.py $exec_quant/iso_tpm.txt -c Cer_CT58 Cer_CT64 -o $exec_quant/Cer_58_64_iso.tpm
        python split_file.py $evts/events.psi -c Cer_CT58 Cer_CT64 -o $exec_quant/Cer_58_64_events.psi
        
        python split_file.py $exec_quant/iso_tpm.txt -c Liv_CT22 Liv_CT28 -o $exec_quant/Liv_22_28_iso.tpm
        python split_file.py $evts/events.psi -c  Liv_CT22 Liv_CT28  -o $exec_quant/Liv_22_28_events.psi

        python split_file.py $exec_quant/iso_tpm.txt -c Liv_CT34 Liv_CT40 -o $exec_quant/Liv_34_40_iso.tpm
        python split_file.py $evts/events.psi -c Liv_CT34 Liv_CT40 -o $exec_quant/Liv_34_40_events.psi

        python split_file.py $exec_quant/iso_tpm.txt -c Liv_CT46 Liv_CT52 -o $exec_quant/Liv_46_52_iso.tpm
        python split_file.py $evts/events.psi -c  Liv_CT46 Liv_CT52  -o $exec_quant/Liv_46_52_events.psi

        python split_file.py $exec_quant/iso_tpm.txt -c Liv_CT58 Liv_CT64 -o $exec_quant/Liv_58_64_iso.tpm
        python split_file.py $evts/events.psi -c Liv_CT58 Liv_CT64 -o $exec_quant/Liv_58_64_events.psi



        echo NOSIGNAL Bio Replica Cer
	python ~/software/suppa/suppa.py diffSplice -m empirical -i $s_events/summary.events_formatted.ioe -e $exec_quant/Cer_22_28_iso.tpm $exec_quant/Cer_46_52_iso.tpm -p $exec_quant/Cer_22_28_events.psi $exec_quant/Cer_46_52_events.psi -o $outDir/nosignal_Cer1
	python ~/software/suppa/suppa.py diffSplice -m empirical -i $s_events/summary.events_formatted.ioe -e $exec_quant/Cer_34_40_iso.tpm $exec_quant/Cer_58_64_iso.tpm -p $exec_quant/Cer_34_40_events.psi $exec_quant/Cer_58_64_events.psi -o $outDir/nosignal_Cer2

        echo NOSIGNAL Bio Replica Liv
	python ~/software/suppa/suppa.py diffSplice -m empirical -i $s_events/summary.events_formatted.ioe -e $exec_quant/Liv_22_28_iso.tpm $exec_quant/Liv_46_52_iso.tpm -p $exec_quant/Liv_22_28_events.psi $exec_quant/Liv_46_52_events.psi -o $outDir/nosignal_Liv1
	python ~/software/suppa/suppa.py diffSplice -m empirical -i $s_events/summary.events_formatted.ioe -e $exec_quant/Liv_34_40_iso.tpm $exec_quant/Liv_58_64_iso.tpm -p $exec_quant/Liv_34_40_events.psi $exec_quant/Liv_58_64_events.psi -o $outDir/nosignal_Liv2
	
	python ~/software/suppa/suppa.py diffSplice -m empirical -i $s_events/summary.events_formatted.ioe -e $exec_quant/Cer_22_28_iso.tpm $exec_quant/Liv_22_28_iso.tpm -p $exec_quant/Cer_22_28_events.psi $exec_quant/Liv_22_28_events.psi -o $outDir/nosignal_control1
	python ~/software/suppa/suppa.py diffSplice -m empirical -i $s_events/summary.events_formatted.ioe -e $exec_quant/Cer_34_40_iso.tpm $exec_quant/Liv_34_40_iso.tpm -p $exec_quant/Cer_34_40_events.psi $exec_quant/Liv_34_40_events.psi -o $outDir/nosignal_control2
        
