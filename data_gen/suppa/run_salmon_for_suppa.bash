#salmon index -t annotation/refseq_mm10_unique.fa -i salmon_index

#1.2: Do the quantification (6 samples):

mkdir -p quantification/1.0
mkdir -p quantification/0.25
mkdir -p quantification/0.5
mkdir -p quantification/MGP

/data/norton_et_al_2017/suppa/salmon_index
#Cer

salmon quant -i /data/norton_et_al_2017/suppa/salmon_index -l ISF -1 /data/Hogenesch/fastq/Cer_CT22_R1.fastq -2 /data/Hogenesch/fastq/Cer_CT22_R2.fastq -p 8 -o ./quantification/1.0/Cer_CT22
salmon quant -i /data/norton_et_al_2017/suppa/salmon_index -l ISF -1 /data/Hogenesch/fastq/Cer_CT28_R1.fastq -2 /data/Hogenesch/fastq/Cer_CT28_R2.fastq -p 8 -o ./quantification/1.0/Cer_CT28
salmon quant -i /data/norton_et_al_2017/suppa/salmon_index -l ISF -1 /data/Hogenesch/fastq/Cer_CT34_R1.fastq -2 /data/Hogenesch/fastq/Cer_CT34_R2.fastq -p 8 -o ./quantification/1.0/Cer_CT34
salmon quant -i /data/norton_et_al_2017/suppa/salmon_index -l ISF -1 /data/Hogenesch/fastq/Cer_CT40_R1.fastq -2 /data/Hogenesch/fastq/Cer_CT40_R2.fastq -p 8 -o ./quantification/1.0/Cer_CT40
salmon quant -i /data/norton_et_al_2017/suppa/salmon_index -l ISF -1 /data/Hogenesch/fastq/Cer_CT46_R1.fastq -2 /data/Hogenesch/fastq/Cer_CT46_R2.fastq -p 8 -o ./quantification/1.0/Cer_CT46
salmon quant -i /data/norton_et_al_2017/suppa/salmon_index -l ISF -1 /data/Hogenesch/fastq/Cer_CT52_R1.fastq -2 /data/Hogenesch/fastq/Cer_CT52_R2.fastq -p 8 -o ./quantification/1.0/Cer_CT52
salmon quant -i /data/norton_et_al_2017/suppa/salmon_index -l ISF -1 /data/Hogenesch/fastq/Cer_CT58_R1.fastq -2 /data/Hogenesch/fastq/Cer_CT58_R2.fastq -p 8 -o ./quantification/1.0/Cer_CT58
salmon quant -i /data/norton_et_al_2017/suppa/salmon_index -l ISF -1 /data/Hogenesch/fastq/Cer_CT64_R1.fastq -2 /data/Hogenesch/fastq/Cer_CT64_R2.fastq -p 8 -o ./quantification/1.0/Cer_CT64

#LIV
salmon quant -i /data/norton_et_al_2017/suppa/salmon_index -l ISF -1 /data/Hogenesch/fastq/Liv_CT22_R1.fastq -2 /data/Hogenesch/fastq/Liv_CT22_R2.fastq -p 8 -o ./quantification/1.0/Liv_CT22
salmon quant -i /data/norton_et_al_2017/suppa/salmon_index -l ISF -1 /data/Hogenesch/fastq/Liv_CT28_R1.fastq -2 /data/Hogenesch/fastq/Liv_CT28_R2.fastq -p 8 -o ./quantification/1.0/Liv_CT28
salmon quant -i /data/norton_et_al_2017/suppa/salmon_index -l ISF -1 /data/Hogenesch/fastq/Liv_CT34_R1.fastq -2 /data/Hogenesch/fastq/Liv_CT34_R2.fastq -p 8 -o ./quantification/1.0/Liv_CT34
salmon quant -i /data/norton_et_al_2017/suppa/salmon_index -l ISF -1 /data/Hogenesch/fastq/Liv_CT40_R1.fastq -2 /data/Hogenesch/fastq/Liv_CT40_R2.fastq -p 8 -o ./quantification/1.0/Liv_CT40
salmon quant -i /data/norton_et_al_2017/suppa/salmon_index -l ISF -1 /data/Hogenesch/fastq/Liv_CT46_R1.fastq -2 /data/Hogenesch/fastq/Liv_CT46_R2.fastq -p 8 -o ./quantification/1.0/Liv_CT46
salmon quant -i /data/norton_et_al_2017/suppa/salmon_index -l ISF -1 /data/Hogenesch/fastq/Liv_CT52_R1.fastq -2 /data/Hogenesch/fastq/Liv_CT52_R2.fastq -p 8 -o ./quantification/1.0/Liv_CT52
salmon quant -i /data/norton_et_al_2017/suppa/salmon_index -l ISF -1 /data/Hogenesch/fastq/Liv_CT58_R1.fastq -2 /data/Hogenesch/fastq/Liv_CT58_R2.fastq -p 8 -o ./quantification/1.0/Liv_CT58
salmon quant -i /data/norton_et_al_2017/suppa/salmon_index -l ISF -1 /data/Hogenesch/fastq/Liv_CT64_R1.fastq -2 /data/Hogenesch/fastq/Liv_CT64_R2.fastq -p 8 -o ./quantification/1.0/Liv_CT64

#Mus
salmon quant -i /data/norton_et_al_2017/suppa/salmon_index -l ISF -1 /data/Hogenesch/fastq/Mus_CT22_R1.fastq -2 /data/Hogenesch/fastq/Mus_CT22_R2.fastq -p 8 -o ./quantification/1.0/Mus_CT22
salmon quant -i /data/norton_et_al_2017/suppa/salmon_index -l ISF -1 /data/Hogenesch/fastq/Mus_CT28_R1.fastq -2 /data/Hogenesch/fastq/Mus_CT28_R2.fastq -p 8 -o ./quantification/1.0/Mus_CT28
salmon quant -i /data/norton_et_al_2017/suppa/salmon_index -l ISF -1 /data/Hogenesch/fastq/Mus_CT34_R1.fastq -2 /data/Hogenesch/fastq/Mus_CT34_R2.fastq -p 8 -o ./quantification/1.0/Mus_CT34
salmon quant -i /data/norton_et_al_2017/suppa/salmon_index -l ISF -1 /data/Hogenesch/fastq/Mus_CT40_R1.fastq -2 /data/Hogenesch/fastq/Mus_CT40_R2.fastq -p 8 -o ./quantification/1.0/Mus_CT40
salmon quant -i /data/norton_et_al_2017/suppa/salmon_index -l ISF -1 /data/Hogenesch/fastq/Mus_CT46_R1.fastq -2 /data/Hogenesch/fastq/Mus_CT46_R2.fastq -p 8 -o ./quantification/1.0/Mus_CT46
salmon quant -i /data/norton_et_al_2017/suppa/salmon_index -l ISF -1 /data/Hogenesch/fastq/Mus_CT52_R1.fastq -2 /data/Hogenesch/fastq/Mus_CT52_R2.fastq -p 8 -o ./quantification/1.0/Mus_CT52
salmon quant -i /data/norton_et_al_2017/suppa/salmon_index -l ISF -1 /data/Hogenesch/fastq/Mus_CT58_R1.fastq -2 /data/Hogenesch/fastq/Mus_CT58_R2.fastq -p 8 -o ./quantification/1.0/Mus_CT58
salmon quant -i /data/norton_et_al_2017/suppa/salmon_index -l ISF -1 /data/Hogenesch/fastq/Mus_CT64_R1.fastq -2 /data/Hogenesch/fastq/Mus_CT64_R2.fastq -p 8 -o ./quantification/1.0/Mus_CT64


##MGP HEART
salmon quant -i /data/norton_et_al_2017/suppa/salmon_index -l ISF -1 /data/MGP/fastq/Lung1_1.fastq -2 /data/MGP/fastq/Lung1_2.fastq -p 8 -o ./quantification/MGP/Lung1
salmon quant -i /data/norton_et_al_2017/suppa/salmon_index -l ISF -1 /data/MGP/fastq/Lung2_1.fastq -2 /data/MGP/fastq/Lung2_2.fastq -p 8 -o ./quantification/MGP/Lung2
salmon quant -i /data/norton_et_al_2017/suppa/salmon_index -l ISF -1 /data/MGP/fastq/Lung3_1.fastq -2 /data/MGP/fastq/Lung3_2.fastq -p 8 -o ./quantification/MGP/Lung3
salmon quant -i /data/norton_et_al_2017/suppa/salmon_index -l ISF -1 /data/MGP/fastq/Lung4_1.fastq -2 /data/MGP/fastq/Lung4_2.fastq -p 8 -o ./quantification/MGP/Lung4
salmon quant -i /data/norton_et_al_2017/suppa/salmon_index -l ISF -1 /data/MGP/fastq/Lung5_1.fastq -2 /data/MGP/fastq/Lung5_2.fastq -p 8 -o ./quantification/MGP/Lung5
salmon quant -i /data/norton_et_al_2017/suppa/salmon_index -l ISF -1 /data/MGP/fastq/Lung6_1.fastq -2 /data/MGP/fastq/Lung6_2.fastq -p 8 -o ./quantification/MGP/Lung6

#MGP LIVER
salmon quant -i /data/norton_et_al_2017/suppa/salmon_index -l ISF -1 /data/MGP/fastq/Liver1_1.fastq -2 /data/MGP/fastq/Liver1_2.fastq -p 8 -o ./quantification/MGP/Liv1
salmon quant -i /data/norton_et_al_2017/suppa/salmon_index -l ISF -1 /data/MGP/fastq/Liver2_1.fastq -2 /data/MGP/fastq/Liver2_2.fastq -p 8 -o ./quantification/MGP/Liv2
salmon quant -i /data/norton_et_al_2017/suppa/salmon_index -l ISF -1 /data/MGP/fastq/Liver3_1.fastq -2 /data/MGP/fastq/Liver3_2.fastq -p 8 -o ./quantification/MGP/Liv3
salmon quant -i /data/norton_et_al_2017/suppa/salmon_index -l ISF -1 /data/MGP/fastq/Liver4_1.fastq -2 /data/MGP/fastq/Liver4_2.fastq -p 8 -o ./quantification/MGP/Liv4
salmon quant -i /data/norton_et_al_2017/suppa/salmon_index -l ISF -1 /data/MGP/fastq/Liver5_1.fastq -2 /data/MGP/fastq/Liver5_2.fastq -p 8 -o ./quantification/MGP/Liv5
salmon quant -i /data/norton_et_al_2017/suppa/salmon_index -l ISF -1 /data/MGP/fastq/Liver6_1.fastq -2 /data/MGP/fastq/Liver6_2.fastq -p 8 -o ./quantification/MGP/Liv6

#MGP Hippocampus
salmon quant -i /data/norton_et_al_2017/suppa/salmon_index -l ISF -1 /data/MGP/fastq/Hippocampus1_1.fastq -2 /data/MGP/fastq/Hippocampus1_2.fastq -p 8 -o ./quantification/MGP/Hip1
salmon quant -i /data/norton_et_al_2017/suppa/salmon_index -l ISF -1 /data/MGP/fastq/Hippocampus2_1.fastq -2 /data/MGP/fastq/Hippocampus2_2.fastq -p 8 -o ./quantification/MGP/Hip2
salmon quant -i /data/norton_et_al_2017/suppa/salmon_index -l ISF -1 /data/MGP/fastq/Hippocampus3_1.fastq -2 /data/MGP/fastq/Hippocampus3_2.fastq -p 8 -o ./quantification/MGP/Hip3
salmon quant -i /data/norton_et_al_2017/suppa/salmon_index -l ISF -1 /data/MGP/fastq/Hippocampus4_1.fastq -2 /data/MGP/fastq/Hippocampus4_2.fastq -p 8 -o ./quantification/MGP/Hip4
salmon quant -i /data/norton_et_al_2017/suppa/salmon_index -l ISF -1 /data/MGP/fastq/Hippocampus5_1.fastq -2 /data/MGP/fastq/Hippocampus5_2.fastq -p 8 -o ./quantification/MGP/Hip5
salmon quant -i /data/norton_et_al_2017/suppa/salmon_index -l ISF -1 /data/MGP/fastq/Hippocampus6_1.fastq -2 /data/MGP/fastq/Hippocampus6_2.fastq -p 8 -o ./quantification/MGP/Hip6



#Hogenesch 0.5 and 0.25 subsamples

salmon quant -i /data/norton_et_al_2017/suppa/salmon_index -l ISF -1 /data/Hogenesch/subsample/0.25/fastq/Cer_CT22_R1.fastq -2 /data/Hogenesch/subsample/0.25/fastq/Cer_CT22_R2.fastq -p 8 -o ./quantification/0.25/Cer_CT22
salmon quant -i /data/norton_et_al_2017/suppa/salmon_index -l ISF -1 /data/Hogenesch/subsample/0.25/fastq/Cer_CT28_R1.fastq -2 /data/Hogenesch/subsample/0.25/fastq/Cer_CT28_R2.fastq -p 8 -o ./quantification/0.25/Cer_CT28
salmon quant -i /data/norton_et_al_2017/suppa/salmon_index -l ISF -1 /data/Hogenesch/subsample/0.25/fastq/Cer_CT34_R1.fastq -2 /data/Hogenesch/subsample/0.25/fastq/Cer_CT34_R2.fastq -p 8 -o ./quantification/0.25/Cer_CT34
salmon quant -i /data/norton_et_al_2017/suppa/salmon_index -l ISF -1 /data/Hogenesch/subsample/0.25/fastq/Cer_CT40_R1.fastq -2 /data/Hogenesch/subsample/0.25/fastq/Cer_CT40_R2.fastq -p 8 -o ./quantification/0.25/Cer_CT40
salmon quant -i /data/norton_et_al_2017/suppa/salmon_index -l ISF -1 /data/Hogenesch/subsample/0.25/fastq/Cer_CT46_R1.fastq -2 /data/Hogenesch/subsample/0.25/fastq/Cer_CT46_R2.fastq -p 8 -o ./quantification/0.25/Cer_CT46
salmon quant -i /data/norton_et_al_2017/suppa/salmon_index -l ISF -1 /data/Hogenesch/subsample/0.25/fastq/Cer_CT52_R1.fastq -2 /data/Hogenesch/subsample/0.25/fastq/Cer_CT52_R2.fastq -p 8 -o ./quantification/0.25/Cer_CT52
salmon quant -i /data/norton_et_al_2017/suppa/salmon_index -l ISF -1 /data/Hogenesch/subsample/0.25/fastq/Cer_CT58_R1.fastq -2 /data/Hogenesch/subsample/0.25/fastq/Cer_CT58_R2.fastq -p 8 -o ./quantification/0.25/Cer_CT58
salmon quant -i /data/norton_et_al_2017/suppa/salmon_index -l ISF -1 /data/Hogenesch/subsample/0.25/fastq/Cer_CT64_R1.fastq -2 /data/Hogenesch/subsample/0.25/fastq/Cer_CT64_R2.fastq -p 8 -o ./quantification/0.25/Cer_CT64

salmon quant -i /data/norton_et_al_2017/suppa/salmon_index -l ISF -1 /data/Hogenesch/subsample/0.25/fastq/Liv_CT22_R1.fastq -2 /data/Hogenesch/subsample/0.25/fastq/Liv_CT22_R2.fastq -p 8 -o ./quantification/0.25/Liv_CT22
salmon quant -i /data/norton_et_al_2017/suppa/salmon_index -l ISF -1 /data/Hogenesch/subsample/0.25/fastq/Liv_CT28_R1.fastq -2 /data/Hogenesch/subsample/0.25/fastq/Liv_CT28_R2.fastq -p 8 -o ./quantification/0.25/Liv_CT28
salmon quant -i /data/norton_et_al_2017/suppa/salmon_index -l ISF -1 /data/Hogenesch/subsample/0.25/fastq/Liv_CT34_R1.fastq -2 /data/Hogenesch/subsample/0.25/fastq/Liv_CT34_R2.fastq -p 8 -o ./quantification/0.25/Liv_CT34
salmon quant -i /data/norton_et_al_2017/suppa/salmon_index -l ISF -1 /data/Hogenesch/subsample/0.25/fastq/Liv_CT40_R1.fastq -2 /data/Hogenesch/subsample/0.25/fastq/Liv_CT40_R2.fastq -p 8 -o ./quantification/0.25/Liv_CT40
salmon quant -i /data/norton_et_al_2017/suppa/salmon_index -l ISF -1 /data/Hogenesch/subsample/0.25/fastq/Liv_CT46_R1.fastq -2 /data/Hogenesch/subsample/0.25/fastq/Liv_CT46_R2.fastq -p 8 -o ./quantification/0.25/Liv_CT46
salmon quant -i /data/norton_et_al_2017/suppa/salmon_index -l ISF -1 /data/Hogenesch/subsample/0.25/fastq/Liv_CT52_R1.fastq -2 /data/Hogenesch/subsample/0.25/fastq/Liv_CT52_R2.fastq -p 8 -o ./quantification/0.25/Liv_CT52
salmon quant -i /data/norton_et_al_2017/suppa/salmon_index -l ISF -1 /data/Hogenesch/subsample/0.25/fastq/Liv_CT58_R1.fastq -2 /data/Hogenesch/subsample/0.25/fastq/Liv_CT58_R2.fastq -p 8 -o ./quantification/0.25/Liv_CT58
salmon quant -i /data/norton_et_al_2017/suppa/salmon_index -l ISF -1 /data/Hogenesch/subsample/0.25/fastq/Liv_CT64_R1.fastq -2 /data/Hogenesch/subsample/0.25/fastq/Liv_CT64_R2.fastq -p 8 -o ./quantification/0.25/Liv_CT64



# Salmon 0.5
salmon quant -i /data/norton_et_al_2017/suppa/salmon_index -l ISF -1 /data/Hogenesch/subsample/0.5/fastq/Cer_CT22_R1_1.fastq -2 /data/Hogenesch/subsample/0.5/fastq/Cer_CT22_R2_1.fastq -p 8 -o ./quantification/0.5/Cer_CT22_1
salmon quant -i /data/norton_et_al_2017/suppa/salmon_index -l ISF -1 /data/Hogenesch/subsample/0.5/fastq/Cer_CT28_R1_1.fastq -2 /data/Hogenesch/subsample/0.5/fastq/Cer_CT28_R2_1.fastq -p 8 -o ./quantification/0.5/Cer_CT28_1
salmon quant -i /data/norton_et_al_2017/suppa/salmon_index -l ISF -1 /data/Hogenesch/subsample/0.5/fastq/Cer_CT34_R1_1.fastq -2 /data/Hogenesch/subsample/0.5/fastq/Cer_CT34_R2_1.fastq -p 8 -o ./quantification/0.5/Cer_CT34_1
salmon quant -i /data/norton_et_al_2017/suppa/salmon_index -l ISF -1 /data/Hogenesch/subsample/0.5/fastq/Cer_CT40_R1_1.fastq -2 /data/Hogenesch/subsample/0.5/fastq/Cer_CT40_R2_1.fastq -p 8 -o ./quantification/0.5/Cer_CT40_1
salmon quant -i /data/norton_et_al_2017/suppa/salmon_index -l ISF -1 /data/Hogenesch/subsample/0.5/fastq/Cer_CT46_R1_1.fastq -2 /data/Hogenesch/subsample/0.5/fastq/Cer_CT46_R2_1.fastq -p 8 -o ./quantification/0.5/Cer_CT46_1
salmon quant -i /data/norton_et_al_2017/suppa/salmon_index -l ISF -1 /data/Hogenesch/subsample/0.5/fastq/Cer_CT52_R1_1.fastq -2 /data/Hogenesch/subsample/0.5/fastq/Cer_CT52_R2_1.fastq -p 8 -o ./quantification/0.5/Cer_CT52_1
salmon quant -i /data/norton_et_al_2017/suppa/salmon_index -l ISF -1 /data/Hogenesch/subsample/0.5/fastq/Cer_CT58_R1_1.fastq -2 /data/Hogenesch/subsample/0.5/fastq/Cer_CT58_R2_1.fastq -p 8 -o ./quantification/0.5/Cer_CT58_1
salmon quant -i /data/norton_et_al_2017/suppa/salmon_index -l ISF -1 /data/Hogenesch/subsample/0.5/fastq/Cer_CT64_R1_1.fastq -2 /data/Hogenesch/subsample/0.5/fastq/Cer_CT64_R2_1.fastq -p 8 -o ./quantification/0.5/Cer_CT64_1

salmon quant -i /data/norton_et_al_2017/suppa/salmon_index -l ISF -1 /data/Hogenesch/subsample/0.5/fastq/Cer_CT22_R1_2.fastq -2 /data/Hogenesch/subsample/0.5/fastq/Cer_CT22_R2_2.fastq -p 8 -o ./quantification/0.5/Cer_CT22_2
salmon quant -i /data/norton_et_al_2017/suppa/salmon_index -l ISF -1 /data/Hogenesch/subsample/0.5/fastq/Cer_CT28_R1_2.fastq -2 /data/Hogenesch/subsample/0.5/fastq/Cer_CT28_R2_2.fastq -p 8 -o ./quantification/0.5/Cer_CT28_2
salmon quant -i /data/norton_et_al_2017/suppa/salmon_index -l ISF -1 /data/Hogenesch/subsample/0.5/fastq/Cer_CT34_R1_2.fastq -2 /data/Hogenesch/subsample/0.5/fastq/Cer_CT34_R2_2.fastq -p 8 -o ./quantification/0.5/Cer_CT34_2
salmon quant -i /data/norton_et_al_2017/suppa/salmon_index -l ISF -1 /data/Hogenesch/subsample/0.5/fastq/Cer_CT40_R1_2.fastq -2 /data/Hogenesch/subsample/0.5/fastq/Cer_CT40_R2_2.fastq -p 8 -o ./quantification/0.5/Cer_CT40_2
salmon quant -i /data/norton_et_al_2017/suppa/salmon_index -l ISF -1 /data/Hogenesch/subsample/0.5/fastq/Cer_CT46_R1_2.fastq -2 /data/Hogenesch/subsample/0.5/fastq/Cer_CT46_R2_2.fastq -p 8 -o ./quantification/0.5/Cer_CT46_2
salmon quant -i /data/norton_et_al_2017/suppa/salmon_index -l ISF -1 /data/Hogenesch/subsample/0.5/fastq/Cer_CT52_R1_2.fastq -2 /data/Hogenesch/subsample/0.5/fastq/Cer_CT52_R2_2.fastq -p 8 -o ./quantification/0.5/Cer_CT52_2
salmon quant -i /data/norton_et_al_2017/suppa/salmon_index -l ISF -1 /data/Hogenesch/subsample/0.5/fastq/Cer_CT58_R1_2.fastq -2 /data/Hogenesch/subsample/0.5/fastq/Cer_CT58_R2_2.fastq -p 8 -o ./quantification/0.5/Cer_CT58_2
salmon quant -i /data/norton_et_al_2017/suppa/salmon_index -l ISF -1 /data/Hogenesch/subsample/0.5/fastq/Cer_CT64_R1_2.fastq -2 /data/Hogenesch/subsample/0.5/fastq/Cer_CT64_R2_2.fastq -p 8 -o ./quantification/0.5/Cer_CT64_2

salmon quant -i /data/norton_et_al_2017/suppa/salmon_index -l ISF -1 /data/Hogenesch/subsample/0.5/fastq/Liv_CT22_R1_1.fastq -2 /data/Hogenesch/subsample/0.5/fastq/Liv_CT22_R2_1.fastq -p 8 -o ./quantification/0.5/Liv_CT22_1
salmon quant -i /data/norton_et_al_2017/suppa/salmon_index -l ISF -1 /data/Hogenesch/subsample/0.5/fastq/Liv_CT28_R1_1.fastq -2 /data/Hogenesch/subsample/0.5/fastq/Liv_CT28_R2_1.fastq -p 8 -o ./quantification/0.5/Liv_CT28_1
salmon quant -i /data/norton_et_al_2017/suppa/salmon_index -l ISF -1 /data/Hogenesch/subsample/0.5/fastq/Liv_CT34_R1_1.fastq -2 /data/Hogenesch/subsample/0.5/fastq/Liv_CT34_R2_1.fastq -p 8 -o ./quantification/0.5/Liv_CT34_1
salmon quant -i /data/norton_et_al_2017/suppa/salmon_index -l ISF -1 /data/Hogenesch/subsample/0.5/fastq/Liv_CT40_R1_1.fastq -2 /data/Hogenesch/subsample/0.5/fastq/Liv_CT40_R2_1.fastq -p 8 -o ./quantification/0.5/Liv_CT40_1
salmon quant -i /data/norton_et_al_2017/suppa/salmon_index -l ISF -1 /data/Hogenesch/subsample/0.5/fastq/Liv_CT46_R1_1.fastq -2 /data/Hogenesch/subsample/0.5/fastq/Liv_CT46_R2_1.fastq -p 8 -o ./quantification/0.5/Liv_CT46_1
salmon quant -i /data/norton_et_al_2017/suppa/salmon_index -l ISF -1 /data/Hogenesch/subsample/0.5/fastq/Liv_CT52_R1_1.fastq -2 /data/Hogenesch/subsample/0.5/fastq/Liv_CT52_R2_1.fastq -p 8 -o ./quantification/0.5/Liv_CT52_1
salmon quant -i /data/norton_et_al_2017/suppa/salmon_index -l ISF -1 /data/Hogenesch/subsample/0.5/fastq/Liv_CT58_R1_1.fastq -2 /data/Hogenesch/subsample/0.5/fastq/Liv_CT58_R2_1.fastq -p 8 -o ./quantification/0.5/Liv_CT58_1
salmon quant -i /data/norton_et_al_2017/suppa/salmon_index -l ISF -1 /data/Hogenesch/subsample/0.5/fastq/Liv_CT64_R1_1.fastq -2 /data/Hogenesch/subsample/0.5/fastq/Liv_CT64_R2_1.fastq -p 8 -o ./quantification/0.5/Liv_CT64_1

salmon quant -i /data/norton_et_al_2017/suppa/salmon_index -l ISF -1 /data/Hogenesch/subsample/0.5/fastq/Liv_CT22_R1_2.fastq -2 /data/Hogenesch/subsample/0.5/fastq/Liv_CT22_R2_2.fastq -p 8 -o ./quantification/0.5/Liv_CT22_2
salmon quant -i /data/norton_et_al_2017/suppa/salmon_index -l ISF -1 /data/Hogenesch/subsample/0.5/fastq/Liv_CT28_R1_2.fastq -2 /data/Hogenesch/subsample/0.5/fastq/Liv_CT28_R2_2.fastq -p 8 -o ./quantification/0.5/Liv_CT28_2
salmon quant -i /data/norton_et_al_2017/suppa/salmon_index -l ISF -1 /data/Hogenesch/subsample/0.5/fastq/Liv_CT34_R1_2.fastq -2 /data/Hogenesch/subsample/0.5/fastq/Liv_CT34_R2_2.fastq -p 8 -o ./quantification/0.5/Liv_CT34_2
salmon quant -i /data/norton_et_al_2017/suppa/salmon_index -l ISF -1 /data/Hogenesch/subsample/0.5/fastq/Liv_CT40_R1_2.fastq -2 /data/Hogenesch/subsample/0.5/fastq/Liv_CT40_R2_2.fastq -p 8 -o ./quantification/0.5/Liv_CT40_2
salmon quant -i /data/norton_et_al_2017/suppa/salmon_index -l ISF -1 /data/Hogenesch/subsample/0.5/fastq/Liv_CT46_R1_2.fastq -2 /data/Hogenesch/subsample/0.5/fastq/Liv_CT46_R2_2.fastq -p 8 -o ./quantification/0.5/Liv_CT46_2
salmon quant -i /data/norton_et_al_2017/suppa/salmon_index -l ISF -1 /data/Hogenesch/subsample/0.5/fastq/Liv_CT52_R1_2.fastq -2 /data/Hogenesch/subsample/0.5/fastq/Liv_CT52_R2_2.fastq -p 8 -o ./quantification/0.5/Liv_CT52_2
salmon quant -i /data/norton_et_al_2017/suppa/salmon_index -l ISF -1 /data/Hogenesch/subsample/0.5/fastq/Liv_CT58_R1_2.fastq -2 /data/Hogenesch/subsample/0.5/fastq/Liv_CT58_R2_2.fastq -p 8 -o ./quantification/0.5/Liv_CT58_2
salmon quant -i /data/norton_et_al_2017/suppa/salmon_index -l ISF -1 /data/Hogenesch/subsample/0.5/fastq/Liv_CT64_R1_2.fastq -2 /data/Hogenesch/subsample/0.5/fastq/Liv_CT64_R2_2.fastq -p 8 -o ./quantification/0.5/Liv_CT64_2

