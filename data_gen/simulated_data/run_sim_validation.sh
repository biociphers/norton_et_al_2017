#!/bin/bash
echo "Running Simulation data validation"

echo "\t Merging transcript expression file per simulation"
python validation_tools/merge_column_files.py sim_naive/input/simulated_reads_Hip1_3.trueTranscriptCounts.txt sim_naive/input/simulated_reads_Hip2_3.trueTranscriptCounts.txt sim_naive/input/simulated_reads_Hip4_3.trueTranscriptCounts.txt sim_naive/input/simulated_reads_Hip5_3.trueTranscriptCounts.txt sim_naive/input/simulated_reads_Hip6_3.trueTranscriptCounts.txt sim_naive/input/simulated_reads_Liv1_3.trueTranscriptCounts.txt sim_naive/input/simulated_reads_Liv2_3.trueTranscriptCounts.txt sim_naive/input/simulated_reads_Liv3_3.trueTranscriptCounts.txt sim_naive/input/simulated_reads_Liv4_3.trueTranscriptCounts.txt sim_naive/input/simulated_reads_Liv5_3.trueTranscriptCounts.txt sim_naive/input/simulated_reads_Liv6_3.trueTranscriptCounts.txt --cols Hip1 Hip2 Hip4 Hip5 Hip6 Liv1 Liv2 Liv3 Liv4 Liv5 Liv6 -o dataset3
python validation_tools/merge_column_files.py sim_real/input/simulated_reads_Hip1_4.trueTranscriptCounts.txt sim_real/input/simulated_reads_Hip2_4.trueTranscriptCounts.txt sim_real/input/simulated_reads_Hip4_4.trueTranscriptCounts.txt sim_real/input/simulated_reads_Hip5_4.trueTranscriptCounts.txt sim_real/input/simulated_reads_Hip6_4.trueTranscriptCounts.txt sim_real/input/simulated_reads_Liv1_4.trueTranscriptCounts.txt sim_real/input/simulated_reads_Liv2_4.trueTranscriptCounts.txt sim_real/input/simulated_reads_Liv3_4.trueTranscriptCounts.txt sim_real/input/simulated_reads_Liv4_4.trueTranscriptCounts.txt sim_real/input/simulated_reads_Liv5_4.trueTranscriptCounts.txt sim_real/input/simulated_reads_Liv6_4.trueTranscriptCounts.txt --cols Hip1 Hip2 Hip4 Hip5 Hip6 Liv1 Liv2 Liv3 Liv4 Liv5 Liv6 -o sim_real


echo "\t Generetinc real truth based on each configuration"
python validation_tools/run_validation.py gen_truth ./sim_naive/merged_vals.tsv ./ensembl.mm10.gff3 --ttable translation_table.tsv -o sim_naive/output_1/ --cond1 Hip4 Hip2 Hip1 --cond2 Liv5 Liv4 Liv2 &
python validation_tools/run_validation.py gen_truth ./sim_naive/merged_vals.tsv ./ensembl.mm10.gff3 --ttable translation_table.tsv -o sim_naive/output_2/ --cond1 Hip2 Hip1 Hip4 --cond2 Liv5 Liv1 Liv3 &
python validation_tools/run_validation.py gen_truth ./sim_naive/merged_vals.tsv ./ensembl.mm10.gff3 --ttable translation_table.tsv -o sim_naive/output_3/ --cond1 Hip5 Hip4 Hip1 --cond2 Liv4 Liv6 Liv2 &

python validation_tools/run_validation.py gen_truth ./sim_real/merged_vals.tsv ./ensembl.mm10.gff3 --ttable translation_table.tsv -o sim_real/output_1/ --cond1 Hip4 Hip2 Hip1 --cond2 Liv5 Liv4 Liv2 &
python validation_tools/run_validation.py gen_truth ./sim_real/merged_vals.tsv ./ensembl.mm10.gff3 --ttable translation_table.tsv -o sim_real/output_2/ --cond1 Hip2 Hip1 Hip4 --cond2 Liv5 Liv1 Liv3 &
python validation_tools/run_validation.py gen_truth ./sim_real/merged_vals.tsv ./ensembl.mm10.gff3 --ttable translation_table.tsv -o sim_real/output_3/ --cond1 Hip5 Hip4 Hip1 --cond2 Liv4 Liv6 Liv2

python validation_tools/run_validation.py gen_truth ./sim_real/merged_vals.tsv ./ensembl.mm10.gff3 --ttable translation_table.tsv -o sim_real_swap/output_2vs3_1/ --cond1 Hip2 Hip1 --cond2 Liv5 Liv4 Liv2 &
python validation_tools/run_validation.py gen_truth ./sim_real/merged_vals.tsv ./ensembl.mm10.gff3 --ttable translation_table.tsv -o sim_real_swap/output_2vs3_2/ --cond1 Hip1 Hip4 --cond2 Liv5 Liv4 Liv2 &
python validation_tools/run_validation.py gen_truth ./sim_real/merged_vals.tsv ./ensembl.mm10.gff3 --ttable translation_table.tsv -o sim_real_swap/output_2vs3_3/ --cond1 Hip4 Hip1 --cond2 Liv5 Liv4 Liv2


echo "RUN SYNTHETIC VALIDATION WITH THE TOOLS"
echo "sim_naive: NAIVE"

python validation_tools/run_validation.py validate ./sim_naive/output_1/annot.pkl -t majiq majiq_lw ./majiq/sim_naive/Hip_Liv_1.lw.tsv -t majiq majiq_nw ./majiq/sim_naive/Hip_Liv_1.nw.tsv -t suppa Suppa ./suppa/sim_naive/Hip_Liv_3vs3_1.dpsi.temp.0 -t rmats rMats ./rMATS/sim_naive/Hip_v_Liv_3vs3_1 --ttable translation_table.tsv -o sim_naive/output_1/
python validation_tools/run_validation.py validate ./sim_naive/output_2/annot.pkl -t majiq majiq_lw ./majiq/sim_naive/Hip_Liv_2.lw.tsv -t majiq majiq_nw ./majiq/sim_naive/Hip_Liv_2.nw.tsv -t suppa Suppa ./suppa/sim_naive/Hip_Liv_3vs3_2.dpsi.temp.0 -t rmats rMats ./rMATS/sim_naive/Hip_v_Liv_3vs3_2 --ttable translation_table.tsv -o sim_naive/output_2/
python validation_tools/run_validation.py validate ./sim_naive/output_3/annot.pkl -t majiq majiq_lw ./majiq/sim_naive/Hip_Liv_3.lw.tsv -t majiq majiq_nw ./majiq/sim_naive/Hip_Liv_3.nw.tsv -t suppa Suppa ./suppa/sim_naive/Hip_Liv_3vs3_3.dpsi.temp.0 -t rmats rMats ./rMATS/sim_naive/Hip_v_Liv_3vs3_3 --ttable translation_table.tsv -o sim_naive/output_3/

echo "sim_real: REALISTIC"
python validation_tools/run_validation.py validate ./sim_real/output_1/annot.pkl -t majiq majiq_lw ./majiq/sim_real/Hip_Liv_1.lw.tsv -t majiq majiq_LowAlpha_lw ./majiq/sim_real_low_alpha/Hip_Liv_1.lw.tsv -t majiq majiq_nw ./majiq/sim_real/Hip_Liv_1.nw.tsv -t suppa Suppa ./suppa/sim_real/Hip_Liv_3vs3_1.dpsi.temp.0 -t rmats rMats ./rMATS/sim_real/Hip_v_Liv_3vs3_1 --ttable translation_table.tsv -o sim_real/output_1/
python validation_tools/run_validation.py validate ./sim_real/output_2/annot.pkl -t majiq majiq_lw ./majiq/sim_real/Hip_Liv_2.lw.tsv -t majiq majiq_LowAlpha_lw ./majiq/sim_real_low_alpha/Hip_Liv_2.lw.tsv -t majiq majiq_nw ./majiq/sim_real/Hip_Liv_2.nw.tsv -t suppa Suppa ./suppa/sim_real/Hip_Liv_3vs3_2.dpsi.temp.0 -t rmats rMats ./rMATS/sim_real/Hip_v_Liv_3vs3_2 --ttable translation_table.tsv -o sim_real/output_2/
python validation_tools/run_validation.py validate ./sim_real/output_3/annot.pkl -t majiq majiq_lw ./majiq/sim_real/Hip_Liv_3.lw.tsv -t majiq majiq_LowAlpha_lw ./majiq/sim_real_low_alpha/Hip_Liv_3.lw.tsv -t majiq majiq_nw ./majiq/sim_real/Hip_Liv_3.nw.tsv -t suppa Suppa ./suppa/sim_real/Hip_Liv_3vs3_3.dpsi.temp.0 -t rmats rMats ./rMATS/sim_real/Hip_v_Liv_3vs3_3 --ttable translation_table.tsv -o sim_real/output_3/

echo "sim_real: REALISTIC + SWAP 2vs3"
python validation_tools/run_validation.py validate ./sim_real_swap/output_2vs3_1/annot.pkl -t majiq majiq_lw ./majiq/sim_real_swap/Hip_Liv_1_lw.tsv -t majiq majiq_nw ./majiq/sim_real_swap/Hip_Liv_1_nw.tsv -t suppa Suppa ./suppa/sim_real_swap/Hip_Liv_3vs3_swap_1.dpsi.temp.0 -t rmats rMats ./rMATS/sim_real_swap/swap_1 --ttable translation_table.tsv -o sim_real_swap/output_2vs3_1/
python validation_tools/run_validation.py validate ./sim_real_swap/output_2vs3_2/annot.pkl -t majiq majiq_lw ./majiq/sim_real_swap/Hip_Liv_2_lw.tsv -t majiq majiq_nw ./majiq/sim_real_swap/Hip_Liv_2_nw.tsv -t suppa Suppa ./suppa/sim_real_swap/Hip_Liv_3vs3_swap_2.dpsi.temp.0 -t rmats rMats ./rMATS/sim_real_swap/swap_2 --ttable translation_table.tsv -o sim_real_swap/output_2vs3_2/
python validation_tools/run_validation.py validate ./sim_real_swap/output_2vs3_3/annot.pkl -t majiq majiq_lw ./majiq/sim_real_swap/Hip_Liv_3_lw.tsv -t majiq majiq_nw ./majiq/sim_real_swap/Hip_Liv_3_nw.tsv -t suppa Suppa ./suppa/sim_real_swap/Hip_Liv_3vs3_swap_3.dpsi.temp.0 -t rmats rMats ./rMATS/sim_real_swap/swap_3 --ttable translation_table.tsv -o sim_real_swap/output_2vs3_3/

