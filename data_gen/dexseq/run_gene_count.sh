#generate their flatten gff file from gtf
python ~/R/x86_64-redhat-linux-gnu-library/3.3/DEXSeq/python_scripts/dexseq_prepare_annotation.py Mus_musculus.NCBIM37.67.gtf ensembl.mm10.37.67.flatten.gff
#check chromosome
for ii in Cer Liv Mus;
do 
    for jj in 28 34 40 46 52 58; 
    do
        python DEXSeq/python_scripts/dexseq_count.py -f bam ensembl.mm10.37.67.flatten.gff /data/Hogenesch/bam/$ii\_CT$jj.sorted.bam -p no -r pos -s no gene_counts/1.0/$ii\_CT$jj.gene_count & 
        python DEXSeq/python_scripts/dexseq_count.py -f bam ensembl.mm10.37.67.flatten.gff /data/Hogenesch/subsample/0.5/bam/$ii\_CT$jj\_1.sorted.bam -p no -r pos -s no gene_counts/0.5/$ii\_CT$jj.gene_count & 
        python DEXSeq/python_scripts/dexseq_count.py -f bam ensembl.mm10.37.67.flatten.gff /data/Hogenesch/subsample/0.25/bam/$ii\_CT$jj.sorted.bam -p no -r pos -s no gene_counts/0.25/$ii\_CT$jj.gene_count & 
    done ; 
done

for ii in Lung Liver Hippocampus;
do 
    for jj in 1 2 3 4 5 6 ; 
    do
        echo python ~/R/x86_64-redhat-linux-gnu-library/3.3/DEXSeq/python_scripts/dexseq_count.py -f bam ensembl.mm10.37.67.flatten.gff /data/MGP/bam/$ii$jj.mm10.sorted.bam -p no -r pos -s no gene_counts/MGP/$ii$jj.gene_count ;
    done
done

