suppressPackageStartupMessages( library( "DEXSeq" ) )
inDir = '/data/norton_et_al_2017/dexseq'
flattenedFile = list.files(inDir, pattern="gff$", full.names=TRUE)

inDir2 = '/data/norton_et_al_2017/dexseq/gene_counts/0.25/'
inDir = '/data/norton_et_al_2017/dexseq/subsamples/0.25/'

DPSI_025_1 = data.frame( row.names = c("Cer_CT28", "Cer_CT34", "Cer_CT40", "Liv_CT28", "Liv_CT34", "Liv_CT40"), 
                         condition = c("Cer", "Cer", "Cer", "Liv", "Liv", "Liv" ),
                         libType = c("paired-end", "paired-end","paired-end", "paired-end","paired-end", "paired-end") )
countFile_025_1 = paste(inDir2, rownames(DPSI_025_1), sep="")
countFile_025_1 = paste(countFile_025_1, ".gene_count", sep="")

DPSI_025_2 = data.frame( row.names = c("Cer_CT46", "Cer_CT52", "Cer_CT58", "Liv_CT46", "Liv_CT52", "Liv_CT58"), 
                        condition = c("Cer", "Cer", "Cer", "Liv", "Liv", "Liv" ),
                        libType = c("paired-end", "paired-end","paired-end", "paired-end","paired-end", "paired-end") )
countFile_025_2 = paste(inDir2, rownames(DPSI_025_2), sep="")
countFile_025_2 = paste(countFile_025_2, ".gene_count", sep="")

dxd_dpsi_025_1 = DEXSeqDataSetFromHTSeq( countFile_025_1, sampleData=DPSI_025_1, design= ~ sample + exon + condition:exon, flattenedfile=flattenedFile )
dxd_dpsi_025_2 = DEXSeqDataSetFromHTSeq( countFile_025_2, sampleData=DPSI_025_2, design= ~ sample + exon + condition:exon, flattenedfile=flattenedFile )

dxr = DEXSeq(dxd_dpsi_025_1)
fname = paste(inDir, 'DPSI_025_1.tsv', sep="")
write.table(dxr, file=fname, quote=FALSE, sep='\t', col.names = NA)

dxr = DEXSeq(dxd_dpsi_025_2)
fname = paste(inDir, 'DPSI_025_2.tsv', sep="")
write.table(dxr, file=fname, quote=FALSE, sep='\t', col.names = NA)


inDir2 = '/data/norton_et_al_2017/dexseq/gene_counts/0.5/'
inDir = '/data/norton_et_al_2017/dexseq/subsamples/0.5/'
DPSI_05_1 = data.frame( row.names = c("Cer_CT28", "Cer_CT34", "Cer_CT40", "Liv_CT28", "Liv_CT34", "Liv_CT40"), 
                        condition = c("Cer", "Cer", "Cer", "Liv", "Liv", "Liv" ),
                        libType = c("paired-end", "paired-end","paired-end", "paired-end","paired-end", "paired-end") )
countFile_05_1 = paste(inDir2, rownames(DPSI_05_1), sep="")
countFile_05_1 = paste(countFile_05_1, ".gene_count", sep="")

DPSI_05_2 = data.frame( row.names = c("Cer_CT46", "Cer_CT52", "Cer_CT58", "Liv_CT46", "Liv_CT52", "Liv_CT58"), 
                        condition = c("Cer", "Cer", "Cer", "Liv", "Liv", "Liv" ),
                        libType = c("paired-end", "paired-end","paired-end", "paired-end","paired-end", "paired-end") )
countFile_05_2 = paste(inDir2, rownames(DPSI_05_2), sep="")
countFile_05_2 = paste(countFile_05_2, ".gene_count", sep="")

dxd_dpsi_05_1 = DEXSeqDataSetFromHTSeq( countFile_05_1, sampleData=DPSI_05_1, design= ~ sample + exon + condition:exon, flattenedfile=flattenedFile )
dxd_dpsi_05_2 = DEXSeqDataSetFromHTSeq( countFile_05_2, sampleData=DPSI_05_2, design= ~ sample + exon + condition:exon, flattenedfile=flattenedFile )

dxr = DEXSeq(dxd_dpsi_05_1)
fname = paste(inDir, 'DPSI_05_1.tsv', sep="")
write.table(dxr, file=fname, quote=FALSE, sep='\t', col.names = NA)

dxr = DEXSeq(dxd_dpsi_05_2)
fname = paste(inDir, 'DPSI_05_2.tsv', sep="")
write.table(dxr, file=fname, quote=FALSE, sep='\t', col.names = NA)


inDir2 = '/data/norton_et_al_2017/dexseq/gene_counts/1.0/'
inDir = '/data/norton_et_al_2017/dexseq/subsamples/1.0/'
DPSI_10_1 = data.frame( row.names = c("Cer_CT28", "Cer_CT34", "Cer_CT40", "Liv_CT28", "Liv_CT34", "Liv_CT40"), 
                        condition = c("Cer", "Cer", "Cer", "Liv", "Liv", "Liv" ),
                        libType = c("paired-end", "paired-end","paired-end", "paired-end","paired-end", "paired-end") )
countFile_10_1 = paste(inDir2, rownames(DPSI_10_1), sep="")
countFile_10_1 = paste(countFile_10_1, ".gene_count", sep="")

DPSI_10_2 = data.frame( row.names = c("Cer_CT46", "Cer_CT52", "Cer_CT58", "Liv_CT46", "Liv_CT52", "Liv_CT58"), 
                        condition = c("Cer", "Cer", "Cer", "Liv", "Liv", "Liv" ),
                        libType = c("paired-end", "paired-end","paired-end", "paired-end","paired-end", "paired-end") )
countFile_10_2 = paste(inDir2, rownames(DPSI_10_2), sep="")
countFile_10_2 = paste(countFile_10_2, ".gene_count", sep="")

dxd_dpsi_10_1 = DEXSeqDataSetFromHTSeq( countFile_10_1, sampleData=DPSI_10_1, design= ~ sample + exon + condition:exon, flattenedfile=flattenedFile )
dxd_dpsi_10_2 = DEXSeqDataSetFromHTSeq( countFile_10_2, sampleData=DPSI_10_2, design= ~ sample + exon + condition:exon, flattenedfile=flattenedFile )

dxr = DEXSeq(dxd_dpsi_10_1)
fname = paste(inDir, 'DPSI_10_1.tsv', sep="")
write.table(dxr, file=fname, quote=FALSE, sep='\t', col.names = NA)

dxr = DEXSeq(dxd_dpsi_10_2)
fname = paste(inDir, 'DPSI_10_2.tsv', sep="")
write.table(dxr, file=fname, quote=FALSE, sep='\t', col.names = NA)

